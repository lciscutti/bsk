package bsk;

import static org.junit.Assert.*;

import org.junit.Test;

import tdd.training.bsk.BowlingException;
import tdd.training.bsk.Frame;
import tdd.training.bsk.Game;

public class GameTest {

	@Test
	public void testAddingAFrameToAGameAndReturnIt() throws BowlingException {
		Game game = new Game();
		Frame frame = new Frame(1,5);
		game.addFrame(frame);
		
		assertEquals(frame, game.getFrameAt(0));
	}
	
	@Test
	public void testAddingAllFramesToAGameAndReturnAll() throws BowlingException {
		Game game = new Game();
		Frame frame1 = new Frame(1,5);
		game.addFrame(frame1);
		
		Frame frame2 = new Frame(3,6);
		game.addFrame(frame2);
		Frame frame3 = new Frame(7,2);
		game.addFrame(frame3);
		Frame frame4 = new Frame(3,6);
		game.addFrame(frame4);
		Frame frame5 = new Frame(4,4);
		game.addFrame(frame5);
		Frame frame6 = new Frame(5,3);
		game.addFrame(frame6);
		Frame frame7 = new Frame(3,3);
		game.addFrame(frame7);
		Frame frame8 = new Frame(4,5);
		game.addFrame(frame8);
		Frame frame9 = new Frame(8,1);
		game.addFrame(frame9);
		Frame frame10 = new Frame(2,6);
		game.addFrame(frame10);
		
		
		assertEquals(frame1, game.getFrameAt(0));
		assertEquals(frame2, game.getFrameAt(1));
		assertEquals(frame3, game.getFrameAt(2));
		assertEquals(frame4, game.getFrameAt(3));
		assertEquals(frame5, game.getFrameAt(4));
		assertEquals(frame6, game.getFrameAt(5));
		assertEquals(frame7, game.getFrameAt(6));
		assertEquals(frame8, game.getFrameAt(7));
		assertEquals(frame9, game.getFrameAt(8));
		assertEquals(frame10, game.getFrameAt(9));
	}
	
	@Test
	public void testCalculatingScoreGameIs81() throws BowlingException {
		Game game = new Game();
		Frame frame1 = new Frame(1,5);
		game.addFrame(frame1);
		Frame frame2 = new Frame(3,6);
		game.addFrame(frame2);
		Frame frame3 = new Frame(7,2);
		game.addFrame(frame3);
		Frame frame4 = new Frame(3,6);
		game.addFrame(frame4);
		Frame frame5 = new Frame(4,4);
		game.addFrame(frame5);
		Frame frame6 = new Frame(5,3);
		game.addFrame(frame6);
		Frame frame7 = new Frame(3,3);
		game.addFrame(frame7);
		Frame frame8 = new Frame(4,5);
		game.addFrame(frame8);
		Frame frame9 = new Frame(8,1);
		game.addFrame(frame9);
		Frame frame10 = new Frame(2,6);
		game.addFrame(frame10);
		
		assertEquals(81, game.calculateScore());
	}
	
	@Test
	public void testCalculatingScoreGameWithSpareIs88() throws BowlingException {
		Game game = new Game();
		Frame frame1 = new Frame(1,9);
		game.addFrame(frame1);
		Frame frame2 = new Frame(3,6);
		game.addFrame(frame2);
		Frame frame3 = new Frame(7,2);
		game.addFrame(frame3);
		Frame frame4 = new Frame(3,6);
		game.addFrame(frame4);
		Frame frame5 = new Frame(4,4);
		game.addFrame(frame5);
		Frame frame6 = new Frame(5,3);
		game.addFrame(frame6);
		Frame frame7 = new Frame(3,3);
		game.addFrame(frame7);
		Frame frame8 = new Frame(4,5);
		game.addFrame(frame8);
		Frame frame9 = new Frame(8,1);
		game.addFrame(frame9);
		Frame frame10 = new Frame(2,6);
		game.addFrame(frame10);
		
		assertEquals(88, game.calculateScore());
	}
	
	@Test
	public void testCalculatingScoreGameWithStrikeIs94() throws BowlingException {
		Game game = new Game();
		Frame frame1 = new Frame(10,0);
		game.addFrame(frame1);
		Frame frame2 = new Frame(3,6);
		game.addFrame(frame2);
		Frame frame3 = new Frame(7,2);
		game.addFrame(frame3);
		Frame frame4 = new Frame(3,6);
		game.addFrame(frame4);
		Frame frame5 = new Frame(4,4);
		game.addFrame(frame5);
		Frame frame6 = new Frame(5,3);
		game.addFrame(frame6);
		Frame frame7 = new Frame(3,3);
		game.addFrame(frame7);
		Frame frame8 = new Frame(4,5);
		game.addFrame(frame8);
		Frame frame9 = new Frame(8,1);
		game.addFrame(frame9);
		Frame frame10 = new Frame(2,6);
		game.addFrame(frame10);
		
		assertEquals(94, game.calculateScore());
	}
	
	@Test
	public void testCalculatingScoreGameWithStrikeAndSpareIs103() throws BowlingException {
		Game game = new Game();
		Frame frame1 = new Frame(10,0);
		game.addFrame(frame1);
		Frame frame2 = new Frame(4,6);
		game.addFrame(frame2);
		Frame frame3 = new Frame(7,2);
		game.addFrame(frame3);
		Frame frame4 = new Frame(3,6);
		game.addFrame(frame4);
		Frame frame5 = new Frame(4,4);
		game.addFrame(frame5);
		Frame frame6 = new Frame(5,3);
		game.addFrame(frame6);
		Frame frame7 = new Frame(3,3);
		game.addFrame(frame7);
		Frame frame8 = new Frame(4,5);
		game.addFrame(frame8);
		Frame frame9 = new Frame(8,1);
		game.addFrame(frame9);
		Frame frame10 = new Frame(2,6);
		game.addFrame(frame10);
		
		assertEquals(103, game.calculateScore());
	}
	
	@Test
	public void testCalculatingScoreGameWithMultiStrikeIs112() throws BowlingException {
		Game game = new Game();
		Frame frame1 = new Frame(10,0);
		game.addFrame(frame1);
		Frame frame2 = new Frame(10,0);
		game.addFrame(frame2);
		Frame frame3 = new Frame(7,2);
		game.addFrame(frame3);
		Frame frame4 = new Frame(3,6);
		game.addFrame(frame4);
		Frame frame5 = new Frame(4,4);
		game.addFrame(frame5);
		Frame frame6 = new Frame(5,3);
		game.addFrame(frame6);
		Frame frame7 = new Frame(3,3);
		game.addFrame(frame7);
		Frame frame8 = new Frame(4,5);
		game.addFrame(frame8);
		Frame frame9 = new Frame(8,1);
		game.addFrame(frame9);
		Frame frame10 = new Frame(2,6);
		game.addFrame(frame10);
		
		assertEquals(112, game.calculateScore());
	}
	
	@Test
	public void testCalculatingScoreGameWithMultiSparesIs98() throws BowlingException {
		Game game = new Game();
		Frame frame1 = new Frame(8,2);
		game.addFrame(frame1);
		Frame frame2 = new Frame(5,5);
		game.addFrame(frame2);
		Frame frame3 = new Frame(7,2);
		game.addFrame(frame3);
		Frame frame4 = new Frame(3,6);
		game.addFrame(frame4);
		Frame frame5 = new Frame(4,4);
		game.addFrame(frame5);
		Frame frame6 = new Frame(5,3);
		game.addFrame(frame6);
		Frame frame7 = new Frame(3,3);
		game.addFrame(frame7);
		Frame frame8 = new Frame(4,5);
		game.addFrame(frame8);
		Frame frame9 = new Frame(8,1);
		game.addFrame(frame9);
		Frame frame10 = new Frame(2,6);
		game.addFrame(frame10);
		
		assertEquals(98, game.calculateScore());
	}
	
	@Test
	public void testCalculatingScoreGameWithSpareLastFrameIs90() throws BowlingException {
		Game game = new Game();
		Frame frame1 = new Frame(1,5);
		game.addFrame(frame1);
		Frame frame2 = new Frame(3,6);
		game.addFrame(frame2);
		Frame frame3 = new Frame(7,2);
		game.addFrame(frame3);
		Frame frame4 = new Frame(3,6);
		game.addFrame(frame4);
		Frame frame5 = new Frame(4,4);
		game.addFrame(frame5);
		Frame frame6 = new Frame(5,3);
		game.addFrame(frame6);
		Frame frame7 = new Frame(3,3);
		game.addFrame(frame7);
		Frame frame8 = new Frame(4,5);
		game.addFrame(frame8);
		Frame frame9 = new Frame(8,1);
		game.addFrame(frame9);
		Frame frame10 = new Frame(2,8);
		game.addFrame(frame10);
		game.setFirstBonusThrow(7);
		
		assertEquals(90, game.calculateScore());
	}
	
	@Test
	public void testCalculatingScoreGameWithSpareLastFrameIs92() throws BowlingException {
		Game game = new Game();
		Frame frame1 = new Frame(1,5);
		game.addFrame(frame1);
		Frame frame2 = new Frame(3,6);
		game.addFrame(frame2);
		Frame frame3 = new Frame(7,2);
		game.addFrame(frame3);
		Frame frame4 = new Frame(3,6);
		game.addFrame(frame4);
		Frame frame5 = new Frame(4,4);
		game.addFrame(frame5);
		Frame frame6 = new Frame(5,3);
		game.addFrame(frame6);
		Frame frame7 = new Frame(3,3);
		game.addFrame(frame7);
		Frame frame8 = new Frame(4,5);
		game.addFrame(frame8);
		Frame frame9 = new Frame(8,1);
		game.addFrame(frame9);
		Frame frame10 = new Frame(10,0);
		game.addFrame(frame10);
		game.setFirstBonusThrow(7);
		game.setSecondBonusThrow(2);
		
		assertEquals(92, game.calculateScore());
	}
	
	@Test
	public void testCalculatingScoreGameWithBestScore() throws BowlingException {
		Game game = new Game();
		Frame frame1 = new Frame(10,0);
		game.addFrame(frame1);
		Frame frame2 = new Frame(10,0);
		game.addFrame(frame2);
		Frame frame3 = new Frame(10,0);
		game.addFrame(frame3);
		Frame frame4 = new Frame(10,0);
		game.addFrame(frame4);
		Frame frame5 = new Frame(10,0);
		game.addFrame(frame5);
		Frame frame6 = new Frame(10,0);
		game.addFrame(frame6);
		Frame frame7 = new Frame(10,0);
		game.addFrame(frame7);
		Frame frame8 = new Frame(10,0);
		game.addFrame(frame8);
		Frame frame9 = new Frame(10,0);
		game.addFrame(frame9);
		Frame frame10 = new Frame(10,0);
		game.addFrame(frame10);
		game.setFirstBonusThrow(10);
		game.setSecondBonusThrow(10);
		
		assertEquals(300, game.calculateScore());
	}
	
	
}
