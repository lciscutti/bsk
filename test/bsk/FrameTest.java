package bsk;

import static org.junit.Assert.*;

import org.junit.Test;

import tdd.training.bsk.BowlingException;
import tdd.training.bsk.Frame;


public class FrameTest {

	@Test
	public void testNormalValidFrameFirstThrowandSecondThrow() throws BowlingException{
		Frame frame = new Frame(2,4);
		
		assertEquals(2, frame.getFirstThrow());
		assertEquals(4, frame.getSecondThrow());
	}
	
	@Test
	public void testMinValidFrameFirstThrowandSecondThrow() throws BowlingException{
		Frame frame = new Frame(0,0);
		
		assertEquals(0, frame.getFirstThrow());
		assertEquals(0, frame.getSecondThrow());
	}
	
	@Test
	public void testFirstMaxValidFrameFirstThrowandSecondThrow() throws BowlingException{
		Frame frame = new Frame(10,0);
		
		assertEquals(10, frame.getFirstThrow());
		assertEquals(0, frame.getSecondThrow());
	}
	
	@Test
	public void testSecondMaxValidFrameFirstThrowandSecondThrow() throws BowlingException{
		Frame frame = new Frame(0,10);
		
		assertEquals(0, frame.getFirstThrow());
		assertEquals(10, frame.getSecondThrow());
	}
	
	@Test
	public void testNotValidFrameFirstThrowandSecondThrow() {
		BowlingException bexception = assertThrows(BowlingException.class, () -> {
			@SuppressWarnings("unused")
			Frame frame = new Frame(1,10);
		});
		
		String expectedMessage = "Pins are max 10. There is a problem with pins counting";
	    String actualMessage = bexception.getMessage();

	    assertTrue(actualMessage.contains(expectedMessage));
	}
	
	@Test
	public void testIsGettingScoreCorrectly() throws BowlingException{
		Frame frame = new Frame(2,6);
		
		assertEquals(8, frame.getScore());
	}

	
}
