package tdd.training.bsk;

import java.util.ArrayList;

public class Game {
	private ArrayList<Frame> frames;
	private int firstBonusThrow;
	private int secondBonusThrow;
	/**
	 * It initializes an empty bowling game.
	 */
	public Game() {
		frames = new ArrayList<>();
		firstBonusThrow = 0;
		secondBonusThrow = 0;
	}

	/**
	 * It adds a frame to this game.
	 * 
	 * @param frame The frame.
	 * @throws BowlingException
	 */
	public void addFrame(Frame frame) {
		frames.add(frame);
	}

	/**
	 * It return the i-th frame of this game.
	 * 
	 * @param index The index (ranging from 0 to 9) of the frame.
	 * @return The i-th frame.
	 * @throws BowlingException
	 */
	public Frame getFrameAt(int index) {
		return frames.get(index);	
	}

	/**
	 * It sets the first bonus throw of this game.
	 * 
	 * @param firstBonusThrow The first bonus throw.
	 * @throws BowlingException
	 */
	public void setFirstBonusThrow(int firstBonusThrow) {
		this.firstBonusThrow = firstBonusThrow;
	}

	/**
	 * It sets the second bonus throw of this game.
	 * 
	 * @param secondBonusThrow The second bonus throw.
	 * @throws BowlingException
	 */
	public void setSecondBonusThrow(int secondBonusThrow) {
		this.secondBonusThrow = secondBonusThrow;
	}

	/**
	 * It returns the first bonus throw of this game.
	 * 
	 * @return The first bonus throw.
	 */
	public int getFirstBonusThrow() {
		return firstBonusThrow;
	}

	/**
	 * It returns the second bonus throw of this game.
	 * 
	 * @return The second bonus throw.
	 */
	public int getSecondBonusThrow() {
		return secondBonusThrow;
	}

	/**
	 * It returns the score of this game.
	 * 
	 * @return The score of this game.
	 * @throws BowlingException
	 */
	public int calculateScore(){
		int score = 0;
		
		for(int i=0; i<frames.size(); i++) {
			if(frames.get(i).isSpare()) {	
				frameIsSpare(i);
			}else if(frames.get(i).isStrike()) {
				frameIsStrike(i);
			}
			score = score + frames.get(i).getScore();
		}
		return score;	
	}
	
	private void frameIsSpare(int i) {
		if(i==frames.size()-1) {
			frames.get(i).setBonus(getFirstBonusThrow());
		}else frames.get(i).setBonus(frames.get(i+1).getFirstThrow());
	}
	
	private void frameIsStrike(int i) {
		if(i==frames.size()-1) {
			frames.get(i).setBonus(getFirstBonusThrow()+getSecondBonusThrow());
		}else if(frames.get(i+1).isStrike()) {
			if(i<frames.size()-2) {
				frames.get(i).setBonus(frames.get(i+1).getFirstThrow()+frames.get(i+2).getFirstThrow());
			}else frames.get(i).setBonus(frames.get(i+1).getFirstThrow()+10);
		}
		else frames.get(i).setBonus(frames.get(i+1).getFirstThrow()+frames.get(i+1).getSecondThrow());
	}
	
	

}
